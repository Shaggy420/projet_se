from Function import *
from pynput.keyboard import Key, Controller
from configReader import *
import screen_brightness_control as sbc

# testing variable,
# if set to 1, will use test data
# if set to 0, will try to detect an arduino-connected serial port
#       and read on it
TESTING = 0
keyboard = Controller()
PERIPHERAL = 0
ACTION = 1
VALUE = 2

DIAL_KEYWORD = "dialing"

serial_test = "button3:press\nbutton3:release\nbutton2:press"

test_keymap = get_config()

funcs = {
    "press": Function(1, lambda a: keyboard.press(a[0])),
    "release": Function(1, lambda a: keyboard.release(a[0])),
    "type": Function(1, lambda a: keyboard.type(a[0])),
    "send": Function(1, lambda a: press_and_release(a)),
    "next_track": (Function(0, lambda: keyboard.type(Key.media_next))),
    "previous_track": (Function(0, lambda: keyboard.type(Key.media_previous))),
    "pause/resume track": (Function(0, lambda: keyboard.type(Key.media_play_pause))),
    "mute music sound": (Function(0, lambda: keyboard.type(Key.media_volume_mute)))
}

dialing_funcs = {
    "volume": Function(1, lambda a: print("volume set to :", a)),
    "luminosite": Function(1, lambda a: set_brightness(a))
}

presets = {
    "ZOOM : activer, désactiver son" : {
        "press" : [#lorsqu'on appuie sur le bouton
            {"press": [Key.alt]},
            {"press": ["a"]}
        ],
        "release" : [#lorsqu'on relâche le bouton
            {"release": [Key.alt]},
            {"release": ["a"]}
        ]
    },

    "ZOOM : terminer l'appel" : {
        "press" : [#lorsqu'on appuie sur le bouton
            {"press": [Key.alt]},
            {"press": ["q"]}
        ],
        "release" : [#lorsqu'on relâche le bouton
            {"release": [Key.alt]},
            {"release": ["q"]}
        ]
    },

    "ZOOM : push to talk" : {
        "press" : [#lorsqu'on appuie sur le bouton
                {"press": [Key.alt]},
                {"press": ["a"]},
                {"release": [Key.alt]},
                {"release": ["a"]}
        ],
        "release" : [#lorsqu'on relâche le bouton
                {"press": [Key.alt]},
                {"press": ["a"]},
                {"release": [Key.alt]},
                {"release": ["a"]}
        ]
    },
    
    "ZOOM : partager son écran" : {
        "press": [#lorsqu'on appuie sur le bouton
            {"press": [Key.alt]},
            {"press": ["t"]},
        ],
        "release": [#lorsqu'on relâche le bouton
            {"release": [Key.alt]},
            {"release": ["t"]}
        ]
    },
    
    "ZOOM : enregistrer la réunion" :{
        "press": [
            {"press": [Key.alt]},
            {"press": ["r"]},
        ],
        "release": [
            {"release": [Key.alt]},
            {"release": ["r"]}
        ]
    },
    
    "ZOOM : ouvrir la fenêtre d'invitation" : {
        "press": [
            {"press": [Key.alt]},
            {"press": ["i"]}
        ],
        "release": [
            {"release": [Key.alt]},
            {"release": ["i"]}
        ]
    },
    
    "NAVIGATEUR : fermer l'onglet actif" : {
        "press": [
            {"press": [Key.ctrl]},
            {"press": ["w"]},
        ],
        "release": [
            {"release": [Key.ctrl]},
            {"release": ["w"]}
        ]
    }
}

# function to press a set of keys and then release them all
# @args keys a list of keys to press, those are directly passed to subfunction keyboard.press()
def press_and_release(keys):
    global keyboard
    for key in keys:
        keyboard.press(key)
    for key in keys:
        keyboard.release(key)


def set_brightness(value):
    sbc.fade_brightness(int(value))
    print("brightness set to :", (int(value)), "%")


def get_new_instructions(instruction_list, serial_com):

    if TESTING:
        return
    else:
        temp = str(serial_com.read_until('/'), 'utf-8')
        print(temp)

        if temp and temp.startswith('|') and temp.endswith('/'):
            # parfois on reçoit plusieurs instructions à la suite
            if temp.count('/') > 1 or temp.count('|') > 1:
                temp = temp.replace("/|", '@')
                temp = temp.split('@')
                temp[0] = temp[0][1:]
                temp[-1] = temp[-1][:-1]
                for t in temp:
                    instruction_list.append(t)
            else:
                temp = temp[1:-1]
                instruction_list.append(temp)


def print_error(error_msg):
    assert error_msg is not None
    print("\033[93m", error_msg, "\033[0m")
    exit(-1)
