import serial.tools.list_ports
from customErrors import NoAvailablePortError


# function to detect an eventual serial port connected to an arduino card
# @returns the port to listen/write to or None
def detect_port():
    ports = list(serial.tools.list_ports.comports())
    port_ = None
    for p in ports:
        if "Arduino" in p.description:
            port_ = p.name

    return port_


class SerialHelper:
    def __init__(self):
        print("scanning ports")
        self.__port = detect_port()
        if self.__port is None:
            raise NoAvailablePortError("Couldn't detect an Arduino card on COM ports")

        print("Arduino card detected on port ", self.__port)
        ser = ""

    def getport(self):
        return self.__port
