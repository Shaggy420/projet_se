# Projet SE

## Concept
Le projet XX consiste à réaliser une interface tangible pour interragir facilement avec son ordinateur en programmant des macros, des combinaisons de touches, en liant un bouton à une touche de clavier, etc.

Le projet permettra également, à terme, de gérer l'intensité sonore et lumineuse de l'ordinateur à l'aide de potentiomètres.

## Fonctionnalités

Pour le moment, il est possible (sans interface graphique), 
- d'associer un bouton à une touche de clavier
- d'associer l'appui ou le relâchement d'un bouton à une ou plusieurs actions (appui d'une touche, écriture d'un message, etc).

## Technologies, librairies utilisées

### Logiciel

#### Interface graphique
Pour réaliser l'interface graphique nous avons utilisé Python avec les librairies suivantes :
- Pyqt5, librairie pour faire des interfaces;
- Pynput, pour simuler un clavier côté logiciel;
- Pyserial, pour la communication série;
- PyJson et Pickle pour sauvegarder et restaurer la configuration;
- os pour l'interraction avec le système d'exploitation;
- XX pour contrôler le volume;
- XX pour contrôler la luminosité.

#### Embarqué
Le code du microprocesseur est écrit en C++ type Arduino.

### Matériel
Pour la partie matérielle, nous avons utilisé une carte Arduino Uno pour le prototypage puis un PCB conçu par notre équipe. Pour les composants, nous les avons commandé par l'école sur les sites 
 {liens}.

## Format des messages sur le port série
Pour communiquer avec l'application Python, le dispositif doit envoyer des messages du type:
`périphérique:action`

exemple :`button1:press`

pour le moment les actions reconnues sont : 
- press
- release

## Format du fichier de configuration
Pour sauvegarder la configuration du dispositif, l'application crée automatiquement un fichier qui décrit les actions à effectuer lorsqu'un évènement est capté.
le format est le suivant:
```json
{
    "périphérique": {
        "evenement1": [
            {"action1": ["arguments"]},
            {"action2": ["arguments"]}
        ], 
        "evenement2": [
            {"action1": ["arguments"]},
            {"action2": ["arguments"]}
        ]   
    }
}
```

voici un exemple utilisé lors de tests:

```json
{
    "button1": {
        "press": [
            {"press": ["a"]}
        ],
        "release": [
            {"release": ["a"]},
            {"type": ["yeet"]}
        ]
    },

    "button2": {
        "press": [
            {"press": ["s"]},
            {"release": ["s"]}
        ]
    },

    "button3": {
        "press": [
            {"send": ["a", "b"]}
        ],

        "release": [
            {"type": ["thats fun"]}
        ]
    }
}
```