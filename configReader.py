import json
import copy
from pynput.keyboard import Key

CONFIG_FILE = "config.txt"


special_keys = {
    1: {"name": "alt", "key": Key.alt},
    2: {"name": "alt gr", "key": Key.alt_gr},
    3: {"name": "shift", "key": Key.shift},
    4: {"name": "Control", "key": Key.ctrl},
    5: {"name": "backspace", "key": Key.backspace},
    6: {"name": "Touche cmd ou windows", "key": Key.cmd},
    7: {"name": "suppr", "key": Key.delete},
    8: {"name": "Flèche bas", "key": Key.down},
    9: {"name": "Flèche haut", "key": Key.up},
    10: {"name": "Flèche gauche", "key": Key.left},
    11: {"name": "Flèche droite", "key": Key.right},
    12: {"name": "entrée", "key": Key.enter},
    13: {"name": "échap", "key": Key.esc},
    14: {"name": "tabulation", "key": Key.tab},
    15: {"name": "Verr maj", "key": Key.caps_lock},
    16: {"name": "Fin", "key": Key.end}
}

keycodes = {
    "Key.alt": 1,
    "Key.alt_gr": 2,
    "Key.shift": 3,
    "Key.ctrl": 4,
    "Key.backspace": 5,
    "Key.cmd": 6,
    "Key.delete": 7,
    "Key.down": 8,
    "Key.up": 9,
    "Key.left": 10,
    "Key.right": 11,
    "Key.enter": 12,
    "Key.esc": 13,
    "Key.tab": 14,
    "Key.caps_lock": 15,
    "Key.end": 16
}


def save_config(keymap):
    t_keymap = copy.deepcopy(keymap)
    for periph in t_keymap:
        for event in t_keymap[periph]:
            for action in t_keymap[periph][event]:
                for act in action.keys():
                    for index, arg in enumerate(action[act]):
                        if type(arg) is not str:
                            if str(arg) not in keycodes:
                                print("erreur dans le fichier de configuration: touche inconnue : ", str(arg), keycodes)
                            else:
                                action[act][index] = keycodes[str(arg)]

    f = open(CONFIG_FILE, "w")
    json.dump(t_keymap, f)
    f.close()
    pass


def get_config():
    f = open(CONFIG_FILE, "r")
    configab = json.load(f)
    f.close()

    for periph in configab:
        for event in configab[periph]:
            for action in configab[periph][event]:
                for act in action.keys():
                    for index, arg in enumerate(action[act]):
                        if type(arg) is int:
                            if arg not in special_keys:
                                print("erreur dans le fichier de configuration recup: touche inconnue")
                            else:
                                action[act][index] = special_keys[arg]["key"]

    return configab
