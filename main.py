# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import unittest

import serial.tools.list_ports
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QSpinBox, QProgressBar, QPushButton
from customErrors import InvalidActionError, NoAvailablePortError

from constants import *
from interface.intMain import MainWindow
from serialComm import SerialHelper
import sys
import threading


test_keymap = get_config()


def mainThread():
    try:
        serial_com = SerialHelper()
        serial_com = serial_com.getport()
    except NoAvailablePortError:
        print("Can't find an available port")
        return

    print("\ninitializing serial communication on com port...")

    if TESTING:
        instruction_stack = serial_test.split("\n")
    else:
        instruction_stack = []
        try:
            ser = serial.Serial(port=serial_com, baudrate=9600, bytesize=8, timeout=1, stopbits=serial.STOPBITS_ONE)
        except serial.SerialException:
            print("couldn't open communication with ", serial_com)
            return

    print("\nstarting main loop...")
    while not TESTING or len(instruction_stack):
        get_new_instructions(instruction_stack, ser)
        if not len(instruction_stack):
            continue
        current_instruction = instruction_stack[0].split(":")

        assert current_instruction[PERIPHERAL] in test_keymap, "device absent from config file, couldn't do anything"

        if current_instruction[ACTION] == DIAL_KEYWORD:
            dial_value = test_keymap[current_instruction[PERIPHERAL]][DIAL_KEYWORD]
            if len(dial_value) < 1:
                print("No value linked to this dialing device")
                continue
            else:
                dial_value = list(test_keymap[current_instruction[PERIPHERAL]][DIAL_KEYWORD][0].keys())[0]
                assert dial_value in dialing_funcs, "The linked parameter is not available"

                dialing_funcs[dial_value].__execute__(current_instruction[VALUE])
        else:
            for instruction in test_keymap[current_instruction[PERIPHERAL]][current_instruction[ACTION]]:
                assert len(instruction) == 1, "incorrect config file format : multiple actions in a function object"
                for cmd in instruction.keys():
                    assert cmd in funcs, "device : " + current_instruction[
                        PERIPHERAL] + "\nrequired unavailable function " \
                                      ": " + current_instruction[ACTION]
                    funcs[cmd].__execute__(instruction[cmd])

        instruction_stack.pop(0)


if __name__ == '__main__':
    print("\nstarting main thread...")
    t = threading.Thread(target=mainThread, daemon=True)

    t.start()

    app = QApplication(sys.argv)

    print("\nstarting main interface...")

    win = MainWindow(test_keymap)
    win.show()

    app.exec()

    t.join()
    exit(-1)
