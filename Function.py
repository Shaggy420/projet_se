from customErrors import NotEnoughArgumentsError


class Function:
    def __init__(self, min_args, script):
        self.min_args = min_args
        self.script = script

    def __execute__(self, args):
        if len(args) < self.min_args:
            raise NotEnoughArgumentsError("Il manque des arguments pour ce script")
        self.script(args)
