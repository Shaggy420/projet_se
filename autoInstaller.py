import pip

try:
    import PyQt5
    import screen_brightness_control
    import pynput
    import serial


except ModuleNotFoundError:
    pip.main(['install', 'pyqt5', 'screen-brightness-control', 'pynput', 'pyserial'])
