class NotEnoughArgumentsError(Exception):
    pass


class NoAvailablePortError(Exception):
    pass


class InvalidActionError(Exception):
    pass