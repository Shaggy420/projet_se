import sys
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QMainWindow, QLineEdit, QSpinBox
import constants
from pynput.keyboard import Key
import configReader


class SetJoystick(QMainWindow):
    def setZQSD(self):
        self.config["JUP"] = {
            "press": [{"press": ["z"]}],
            "release": [{"release": ["z"]}]
        }

        self.config["JLEFT"] = {
            "press": [{"press": ["q"]}],
            "release": [{"release": ["q"]}]
        }

        self.config["JRIGHT"] = {
            "press": [{"press": ["d"]}],
            "release": [{"release": ["d"]}]
        }

        self.config["JDOWN"] = {
            "press": [{"press": ["s"]}],
            "release": [{"release": ["s"]}]
        }

        configReader.save_config(self.config)

    def setArrows(self):

        self.config["JUP"] = {
            "press": [{"press": [Key.up]}],
            "release": [{"release": [Key.up]}]
        }

        self.config["JLEFT"] = {
            "press": [{"press": [Key.left]}],
            "release": [{"release": [Key.left]}]
        }

        self.config["JRIGHT"] = {
            "press": [{"press": [Key.right]}],
            "release": [{"release": [Key.right]}]
        }

        self.config["JDOWN"] = {
            "press": [{"press": [Key.down]}],
            "release": [{"release": [Key.down]}]
        }

        configReader.save_config(self.config)

    def __init__(self, config):
        super().__init__()

        self.setWindowTitle("Joystick")
        self.setFixedSize(350, 350)
        self.config = config

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: #333333; color: white; font-size: 22px;')

        cssButton = "background-color: #006e84; font-size: 16px; border-radius: 7px;"
        cssSmallButton = "background-color: #006e84; font-size: 12px; border-radius: 7px;"
        cssLabel = "font-weight: bold;"

        # du texte
        choix1 = QLabel(self)
        choix1.setText("Pré-réglages")
        choix1.setStyleSheet(cssLabel)
        choix1.setFixedSize(170, 24)
        choix1.move(100, 20)
        choix1.show()

        # bouton 1
        button1 = QPushButton(self)
        button1.setText("Flèches directionnelles")
        button1.setStyleSheet(cssButton)
        button1.setFixedSize(300, 50)
        button1.move(25, 50)
        button1.clicked.connect(lambda: self.setArrows())
        button1.show()

        # bouton 2
        button2 = QPushButton(self)
        button2.setText("Touches ZQSD")
        button2.setStyleSheet(cssButton)
        button2.setFixedSize(300, 50)
        button2.move(25, 120)
        button2.clicked.connect(lambda: self.setZQSD())
        button2.show()

        button3 = QPushButton(self)
        button3.setText("Haut")
        button3.setStyleSheet(cssSmallButton)
        button3.setFixedSize(50, 50)
        button3.move(150, 180)
        button3.show()

        button4 = QPushButton(self)
        button4.setText("Gauche")
        button4.setStyleSheet(cssSmallButton)
        button4.setFixedSize(50, 50)
        button4.move(100, 230)
        button4.show()

        button5 = QPushButton(self)
        button5.setText("Droite")
        button5.setStyleSheet(cssSmallButton)
        button5.setFixedSize(50, 50)
        button5.move(200, 230)
        button5.show()

        button6 = QPushButton(self)
        button6.setText("Bas")
        button6.setStyleSheet(cssSmallButton)
        button6.setFixedSize(50, 50)
        button6.move(150, 280)
        button6.show()
