import sys
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QMainWindow, QLineEdit, QSpinBox
import constants
import configReader


class SetButton(QMainWindow):
    def assign(self, periph, value):
        print("appuyé")
        self.config[periph] = value
        configReader.save_config(self.config)

    def __init__(self, config, whichOne):
        self.whichOne = whichOne
        self.config = config
        super().__init__()

        print("Tu as appuyé sur : " + self.whichOne)  # J'ai récupéré le btn sur lequel on a appuyé

        if self.whichOne == "interrupteur":
            self.setWindowTitle("Interrupteur")
        else:
            self.setWindowTitle("Bouton")
        self.setFixedSize(350, 300)
        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: #333333; color: white; font-size: 22px; ')

        cssButton = "background-color: #006e84; font-size: 16px; border-radius: 7px;"
        cssLabel = "font-weight: bold;"

        # du texte
        choix1 = QLabel(self)
        choix1.setText("Pré-réglages")
        choix1.setStyleSheet(cssLabel)
        choix1.setFixedSize(160, 24)
        choix1.move(100, 15)
        choix1.show()

        # bouton 1
        button1 = QPushButton(self)
        button1.setText("Couper le son")
        button1.setStyleSheet(cssButton)
        button1.setFixedSize(300, 50)
        button1.move(25, 50)
        button1.clicked.connect(lambda: (self.assign("lever" if self.whichOne == "interrupteur" else "button1",
                                                     constants.presets["ZOOM : activer, désactiver son"])))
        button1.show()

        # bouton 2
        button2 = QPushButton(self)
        button2.setText("Terminer l'appel")
        button2.setStyleSheet(cssButton)
        button2.setFixedSize(300, 50)
        button2.move(25, 120)
        button2.clicked.connect(lambda: (self.assign("lever" if self.whichOne == "interrupteur" else "button1",
                                                     constants.presets["ZOOM : terminer l'appel"])))
        button2.show()

        # du texte
        choix2 = QLabel(self)
        choix2.setText("Personnalisé")
        choix2.setStyleSheet(cssLabel)
        choix2.setFixedSize(200, 20)
        choix2.move(100, 200)
        choix2.show()

        button3 = QPushButton(self)
        button3.setText("Executer un fichier")
        button3.setStyleSheet(cssButton)
        button3.setFixedSize(300, 50)
        button3.move(25, 230)
        button3.show()

# window = SetButton('button1')
# window.show()

# app.exec()
