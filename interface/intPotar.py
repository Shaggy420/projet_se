import sys
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QMainWindow, QLineEdit, QSpinBox
from PyQt5.QtGui import QPalette, QColor
import constants
import configReader

app = QApplication(sys.argv)




class SetPotar(QMainWindow):  # interface pour parametrer le potentiomètre
    def assign(self, value):
        self.config["dial1"] = value
        configReader.save_config(self.config)

    def __init__(self, config):
        super().__init__()

        self.config = config
        print(id(config["dial1"]), id(self.config["dial1"]))

        self.setWindowTitle("Potentiomètre")
        self.setFixedSize(350, 350)

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: #333333; color: white; font-size: 22px;')

        cssButton = "background-color: #006e84; font-size: 16px; border-radius: 7px;"
        cssLabel = "font-weight: bold;"

        # du texte
        choix1 = QLabel(self)
        choix1.setText("Pré-réglages")
        choix1.setStyleSheet(cssLabel)
        choix1.setFixedSize(160, 24)
        choix1.move(100, 15)
        choix1.show()

        # bouton 1
        button1 = QPushButton(self)
        button1.setText("Réglage du son")
        button1.setStyleSheet(cssButton)
        button1.setFixedSize(300, 50)
        button1.move(25, 50)
        button1.clicked.connect(lambda: (self.assign({"dialing": [{"volume": []}]})))
        button1.show()

        # bouton 2
        button2 = QPushButton(self)
        button2.setText("Réglage de la luminosité")
        button2.setStyleSheet(cssButton)
        button2.setFixedSize(300, 50)
        button2.move(25, 120)
        button2.clicked.connect(lambda: (self.assign({"dialing": [{"luminosite": []}]})))
        button2.show()

        # du texte
        choix2 = QLabel(self)
        choix2.setText("Personnalisé")
        choix2.setStyleSheet(cssLabel)
        choix2.setFixedSize(170, 24)
        choix2.move(90, 200)
        choix2.show()

        persopotar = PersonnaliserPotar(self)
        persopotar.setFixedSize(200, 65)
        persopotar.move(70, 250)
        persopotar.show()


class PersonnaliserPotar(QWidget):  # sous-widget pour personnaliser les actions du potentiomètre
    def __init__(self, parent):
        super(PersonnaliserPotar, self).__init__(parent)

        # self.setAutoFillBackground(False)
        # p = self.palette()
        # p.setColor(self.backgroundRole(), Qt.blue)
        # self.setPalette(p)

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: #006e84 ; border-radius: 5px; color: white; font-size: 14px;')

        txt1 = QLabel(self)
        txt1.setText("Déclencher")
        txt1.move(5, 5)
        txt1.show()

        btn1 = QPushButton(self)
        btn1.setText(" choisir fichier ")
        btn1.setStyleSheet("background-color: #3c3c3c; border-radius: 3px;")
        btn1.clicked.connect(lambda: print("Et là on met un truc pour choisir un fichier"))
        btn1.move(90, 4)
        btn1.show()

        txt2 = QLabel(self)
        txt2.setText("à partir de")
        txt2.move(5, 33)
        txt2.show()

        btn2 = QSpinBox(self)  # en fait c'est plus un btn mais tkt
        btn2.setMaximum(100)
        btn2.setStyleSheet("background-color: #3c3c3c; border-radius: 3px;")
        # btn2.setText("--")
        btn2.move(85, 30)
        btn2.show()

        txt3 = QLabel(self)
        txt3.setText("%")
        txt3.move(140, 33)
        txt3.show()


if __name__ == "__main__":
    window = SetPotar()
    window.show()

    app.exec()
