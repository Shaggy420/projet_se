#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QMainWindow
from subprocess import call

# Subclass QMainWindow to customize your application's main window
from interface import intPotar, intJoystick, intBouton


class MainWindow(QMainWindow):
    def __init__(self, config):
        super().__init__()

        self.setWindowTitle("Interface")
        self.setFixedSize(350, 350)

        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: #333333; color: white; font-size: 22px;')

        cssButton = "background-color: #006e84; font-size: 16px; border-radius: 7px;"
        cssLabel = "font-weight: bold;"

        def openWindow(var):  # ouvre l'interface du correspondant au bouton sur lequel on vient de cliquer
            if var == "potentiometre":
                print("Potentiometre")
                self.makePotar = intPotar.SetPotar(config)
                self.makePotar.show()
            if var == "joystick":
                print("Joystick")
                self.makeJoystick = intJoystick.SetJoystick(config)
                self.makeJoystick.show()
            if var == "interrupteur":
                print("Interrupteur")
                self.makeInterrupteur = intBouton.SetButton(config, "interrupteur")
                self.makeInterrupteur.show()
            if var == "button1":
                print("Bouton 1")
                self.makeButton1 = intBouton.SetButton(config, "button1")
                self.makeButton1.show()

        # Le titre de la fenetre
        titre = QLabel(self)
        titre.setText("Réglages du pédalier")
        titre.setStyleSheet(cssLabel)
        titre.setFixedSize(300, 24)
        titre.move(45, 15)
        titre.show()

        # bouton 1
        button1 = QPushButton(self)
        button1.setText("Potentiomètre")
        button1.setStyleSheet(cssButton)
        button1.setFixedSize(300, 50)
        button1.move(25, 80)
        button1.clicked.connect(lambda: openWindow("potentiometre"))
        button1.show()

        # bouton 2
        button2 = QPushButton(self)
        button2.setText("Joystick")
        button2.setStyleSheet(cssButton)
        button2.clicked.connect(lambda: openWindow("joystick"))
        button2.setFixedSize(300, 50)
        button2.move(25, 145)
        button2.show()
        # bouton 3
        button3 = QPushButton(self)
        button3.setText("Interrupteur")
        button3.setStyleSheet(cssButton)
        button3.setFixedSize(300, 50)
        button3.clicked.connect(lambda: openWindow("interrupteur"))
        button3.move(25, 210)
        button3.show()
        # bouton 4
        button4 = QPushButton(self)
        button4.setText("Bouton 1")
        button4.setStyleSheet(cssButton)
        button4.clicked.connect(lambda: openWindow("button1"))
        button4.setFixedSize(300, 50)
        button4.move(25, 275)
        button4.show()
        # # bouton debug
        # button5 = QPushButton(self)
        # button5.setText("print config")
        # button5.setStyleSheet(cssButton)
        # button5.clicked.connect(lambda: print(config))
        # button5.setFixedSize(300, 50)
        # button5.move(25, 350)
        # button5.show()
